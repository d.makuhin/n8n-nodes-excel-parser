import { INodeExecutionData, INodeType, INodeTypeDescription, NodeOperationError } from 'n8n-workflow';
import { IExecuteFunctions } from 'n8n-core';
import ExcelJs from 'exceljs';

interface CellType {
  cell: string,
  name: string,
  type: 'cell' | 'table',
}

interface CellList {
  cellsList: CellType[]
}

const selectRange = (sheet: ExcelJs.Worksheet, rangeCell: string) => {
  const [startCell, endCell] = rangeCell.split(":")

  const [endCellColumn, endRow] = endCell.match(/[a-z]+|[^a-z]+/gi) as string[]
  const [startCellColumn, startRow] = startCell.match(
    /[a-z]+|[^a-z]+/gi
  ) as string[]

  let endColumn = sheet.getColumn(endCellColumn)
  let startColumn = sheet.getColumn(startCellColumn)

  if (!endColumn) throw Error("End column not found")
  if (!startColumn) throw Error("Start column not found")

  const endColumnNumber = endColumn.number
  const startColumnNumber = startColumn.number

  const cells = []
  for (let y = parseInt(startRow); y <= parseInt(endRow); y++) {
    const row = sheet.getRow(y)

    for (let x = startColumnNumber; x <= endColumnNumber; x++) {
      cells.push(row.getCell(x))
    }
  }

  return cells
}

export class ExcelParser implements INodeType {
  description: INodeTypeDescription = {
    displayName: 'Excel Parser',
    name: 'excelParser',
    icon: 'file:excelparser.svg',
    group: ['transform'],
    version: 1,
    description: 'Excel Parser',
    defaults: {
      name: 'Excel Parser',
    },
    inputs: ['main'],
    outputs: ['main'],
    properties: [
      {
        displayName: 'List in Visible',
        name: 'listId',
        type: 'number',
        default: 1,
        placeholder: 'List serial number',
        description: 'Enter the order of the sheet from those that are visible',
      },
      {
        displayName: 'Cells List',
        name: 'cells',
        type: 'fixedCollection',
        typeOptions: {
          multipleValues: true,
          multipleValueButtonText: 'Add Field',
        },
        default: {},
        description: "The parsing cells",
        options: [
          {
            name: 'cellsList',
            displayName: 'Cell:Name',
            values: [
              {
                displayName: 'Type',
                name: 'type',
                type: 'options',
                options: [
                  {
                    name: 'Cell',
                    value: 'cell',
                  },
                  {
                    name: 'Table',
                    value: 'table',
                  },
                ],
                default: 'cell',
                required: true,
                description: 'Cell Name',
              },
              {
                displayName: 'Cell',
                name: 'cell',
                type: 'string',
                default: '',
                required: true,
                description: 'Cell Name',
              },
              {
                displayName: 'Name',
                name: 'name',
                type: 'string',
                default: '',
                description: 'Name field',
              },
            ],
          },
        ],
      },
    ],
  }

  async execute(this: IExecuteFunctions): Promise<INodeExecutionData[][]> {
    const items = this.getInputData();
    const workbook: ExcelJs.Workbook = new ExcelJs.Workbook();

    let item: INodeExecutionData;
    let cells: CellList;
    let listId: number;

    for (let itemIndex = 0; itemIndex < items.length; itemIndex++) {
      try {
        cells = this.getNodeParameter('cells', itemIndex) as CellList;
        listId = Number(this.getNodeParameter('listId', itemIndex));
        item = items[itemIndex];
        item.json = {};

        if (item.binary) {
          const buffer = Buffer.from(item.binary.data.data, 'base64');
          await workbook.xlsx.load(buffer).then((workbook) => {
            const sheets = workbook.worksheets.filter((item: ExcelJs.Worksheet) => item.state !== 'hidden');
            const sheetId = sheets[listId].id;
            const worksheet = workbook.getWorksheet(sheetId);

            for (let cell of cells.cellsList) {
              if (cell.type === 'cell') {
                const fill = worksheet.getCell(cell.cell).style.fill as ExcelJs.FillPattern;

                item.json[cell.name === '' ? cell.cell : cell.name] = {
                  value: worksheet.getCell(cell.cell).value,
                  color: fill.bgColor,
                };
              }

              if (cell.type === 'table') {
                const table = selectRange(worksheet, cell.cell);

                table.forEach((cell) => {
                  item.json[cell.address] = cell.value;
                })
              }
            }
          });
        }

        item.binary = {};
      } catch (error) {
        if (this.continueOnFail()) {
          items.push({ json: this.getInputData(itemIndex)[0].json, error, pairedItem: itemIndex });
        } else {
          if (error.context) {
            error.context.itemIndex = itemIndex;
            throw error;
          }
          throw new NodeOperationError(this.getNode(), error, {
            itemIndex,
          });
        }
      }
    }

    return this.prepareOutputData(items);
  }
}
